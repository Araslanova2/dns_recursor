# b'\xee\xfa' -> '1110111011111010'
def hex_to_bits(hex_byte_str):
    if type(hex_byte_str) == int:
        return "{:08b}".format(hex_byte_str)

    bits = ""
    for hex_num in hex_byte_str: # byte strings splitted in ints via for-loop
        bits += "{:08b}".format(hex_num)
    return bits

# '1110111011111010' -> b'\xee\xfa'
def bits_to_hex(bit_str):
    assert len(bit_str) % 8 == 0
    bit8 = [bit_str[i : i + 8] for i in range(0, len(bit_str), 8)]
    bit8 = [int(b, base=2).to_bytes(1, byteorder = 'big') for b in bit8]
    hex_byte_str = b''.join(bit8)
    return hex_byte_str

# For more types look this
# https://en.wikipedia.org/wiki/List_of_DNS_record_types
def convert_record_type(query_type):
    query_type = int(hex_to_bits(query_type), base=2)
    if query_type == 1:
        query_type = 'A'
    elif query_type == 28:
        query_type = 'AAAA'
    elif query_type == 2:
        query_type = 'NS'
    return query_type

def convert_record_class(query_class):
    query_class = int(hex_to_bits(query_class), base=2)
    if query_class == 1:
        query_class = 'IN' # internet
    return query_class

# example with fisrt_octat_id = 0
# name = 'abc.qwerty.ru.'
# return {0 : 'abc.qwerty.ru.', 4 : 'qwerty.ru.', 12 : 'ru.'}
def get_octat_to_name(fisrt_octat_id, name):
    name = name.split('.')[:-1]
    octat_to_name = {}
    shift = 0
    for idx in range(len(name)):
        tail_name = name[idx] + '.'
        for tail_idx in range(idx + 1, len(name)):
            tail_name += name[tail_idx] + '.'
        octat_to_name[fisrt_octat_id + shift] = tail_name
        shift += len(name[idx]) + 1
    return octat_to_name

# reads message from answer_body until \0-byte, supports compression
def extract_name(answer_body, fisrt_octat_id, known_octat_to_name):
    cnt_in_name = 0

    name = ""
    while answer_body:
        is_compressed = hex_to_bits(answer_body[:2])
        if is_compressed[:2] == '11': # compressed
            offset = int(is_compressed[2:], base=2)
            if offset in known_octat_to_name:
                name += known_octat_to_name[offset]
            else:
                print("Offset", offset, ' = ', hex_to_bits(answer_body[:2]), "not found in", known_octat_to_name)

            cnt_in_name += 2
            answer_body = answer_body[2:]
            continue

        substr_len = answer_body[0]
        cnt_in_name += substr_len + 1
        if substr_len == 0:
            answer_body = answer_body[1:]
            break

        name += answer_body[1 : substr_len + 1].decode() + '.'
        answer_body = answer_body[substr_len + 1:]
    if not name:
        print("Attention. extract_name didn't extract any name")

    if not name or name[-1] != '.':
        name += '.'

    octat_to_name = get_octat_to_name(fisrt_octat_id, name)
    return (name, answer_body, octat_to_name, cnt_in_name)

# write mesage to byte string (invert operation to the `extract_name`)
def encode_name_to_hex_byte_str(name):
    address = b''
    for subname in name.split('.'):
        address += len(subname).to_bytes(1, byteorder='big')
        address += bytes(bytearray.fromhex(subname.encode("utf-8").hex()))
    address += bits_to_hex('0' * 8)
    return address

# to convert 4 bytes to ipv4 and 16 bytes in ipv6
def read_ip(line_to_parse, ip_length):
    ip = ''
    if ip_length == 4:
        for number in line_to_parse:
            ip += str(number) + '.'
        ip = ip[:-1]
    elif ip_length == 16:
        ip = line_to_parse.hex()
        for i in range(28, 3, -4):
            ip = ip[:i] + ':' + ip[i:]
    else:
        print('Ip address len =', ip_length)
    return ip
