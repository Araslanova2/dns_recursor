import json
import sys
sys.path.append('../')

from crawler import Crawler

# correct names
for website in ['academy.yandex.ru', 'vk.ru', 'google.com', 'web.telegram.org', 'microsoft.com']:
    algo = Crawler()
    history_to_print, ip = algo.find_ip(website, use_cache = True)
    print()
    print(json.dumps(history_to_print, indent=2))

# incorrect names
for website in ['rrrrrrrrrrrrrrr', 'xyz.vk.ru', 'xyz.vk.ru', 'xyz.vk.ru', 'xyz.vk.ru', 'xyz.vk.ru']:
    algo = Crawler()
    history_to_print, ip = algo.find_ip(website, use_cache=True)
    print()
    print(json.dumps(history_to_print, indent=2))
