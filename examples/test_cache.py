import sys
sys.path.append('../')

from cache import Cache

A_record1 = {
      'name': '1',
      "ip_ttl": 600,
      "ip": "93.158.134.200"
    }
A_record2 = {
      'name': '2',
      "ip_ttl": 600,
      "ip": "0.158.134.0"
    }
NS_record = [
      {
        'name': 'record_3',
        "ttl": 345600,
        "ip": [
          {
            "ip_ttl": 345600,
            "ip": "213.180.193.1"
          },
          {
            "ip_ttl": 345600,
            "ip": "2a02:06b8:0000:0000:0000:0000:0000:0001"
          }
        ]
      }
]

storage = Cache(2)
storage.add('1', A_record1, 'A')
storage.add('2', A_record2, 'A')
assert(storage.get('1')[0]['name'] == '1')
storage.add('3', NS_record, 'NS')
assert(len(storage.name_to_record) == 2)
assert(storage.get('3')[0][0]['name'] == 'record_3')
assert(storage.get('3', 'a_record_3')[0][0]['name'] == 'record_3')
storage.set_max_size(1)
assert(len(storage.name_to_record) == 1)
assert(storage.cnt_records_in_cache == 1)

print("OK")
