import socket
import json
import sys
sys.path.append('../')

from create_request import create_request
from parse_response import parse_response

transaction_id, message = create_request("academy.yandex.ru", "NS")

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.sendto(message, ("202.12.27.33", 53))
response, sender = sock.recvfrom(512)
print("received a response from", sender)
print(response)

print()
print("Response parsing results:")
transaction_id, query_info, records = parse_response(response)

print("query_info")
print(json.dumps(query_info, indent=2))

print("records")
print(json.dumps(records, indent=2))

