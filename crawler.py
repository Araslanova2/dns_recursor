import socket

from cache import Cache
from create_request import create_request
from parse_response import parse_response

_ROOT_SERVERS_IPS = [
            "198.41.0.4", # a.root-servers.net.
            "199.9.14.201",
            "192.33.4.12",
            "199.7.91.13",
            "192.203.230.10",
            "192.5.5.241",
            "192.112.36.4"
            "198.97.190.53",
            "192.36.148.17",
            "192.58.128.30",
            "193.0.14.129",
            "199.7.83.42",
            "202.12.27.33"] # m.root-servers.net.

storage = Cache(100)

class Crawler:
    def __init__(self, discovered_ips = None):
        self._history = []
        self._no_ips_yet = []
        self._discovered_ips = set() if discovered_ips is None else discovered_ips
        self.TIMEOUT = 2.0 # in seconds
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def _communicate(self, message, ip):
        try:
            self.sock.sendto(message, (ip, 53))
        except:
            self._history.append({'Result' : 'an error during message sending'})
            return None

        self._discovered_ips.add(ip)
        self.sock.settimeout(self.TIMEOUT)
        try:
            response, sender = self.sock.recvfrom(512)
        except:
            self._history.append({'Result' : f'timeout ({int(self.TIMEOUT)}s)'})
            return None
        return response

    def _get_ips_to_explore(self, records):
        ips_to_explore = []
        for record in records:
            if 'ip' in record:
                for ip_info in record['ip']:
                    if ip_info['ip_type'] == 'A':
                        ips_to_explore.append((ip_info['ip'], record['name']))
            else:
                self._no_ips_yet.append((record['name_server'], record['name']))
        return ips_to_explore

    def _get_request_type(self, prev_found_suffix, website_name):
        if prev_found_suffix == '' or prev_found_suffix.count('.') + 1 < website_name.count('.'):
            return 'NS'
        return 'A'

    def _crawl_ip(self, ip, prev_found_suffix, website_name):
        global storage
        request_type = self._get_request_type(prev_found_suffix, website_name)
        transaction_id, message = create_request(website_name, request_type)

        self._history.append({'Action' : f'Explore {ip} for a name {website_name}'})
        response = self._communicate(message, ip)
        if response is None:
            return None # history already recorded in self._communicate
        transaction_id, query_info, records = parse_response(response)

        if query_info is None:
            self._history.append({'Result' : 'Got an error in a request'})
            return None

        self._history.append({'Result' : records})
        if request_type == 'NS' and 'ip' not in records:
            storage.add(ip, records, 'NS')
        if 'ip' in records: # automatically request_type = 'A'
            storage.add(website_name, records, 'A')
        return records

    def _crawl_recursively(self, website_name, ips_to_explore, use_cache):
        global storage
        if not ips_to_explore:
            return False, None, None
        #ips_to_explore = ips_to_explore[:1]

        for (ip, prev_found_suffix) in ips_to_explore:
            if ip in self._discovered_ips:
                continue

            if use_cache and storage.get(ip, website_name)[0]:
                records = storage.get(ip, website_name)[0]
                self._history.append({'Action' : {
                    f'Result for an ip {ip} extracted from cache' : records}})
            else:
                records = self._crawl_ip(ip, prev_found_suffix, website_name)

            if records is None:
                continue

            if 'ip' in records: # automatically request_type = 'A'
                return True, records['ip'], records['ip_ttl']

            ips_to_explore = self._get_ips_to_explore(records)
            status, ip, ip_ttl = self._crawl_recursively(website_name, ips_to_explore, use_cache)
            if status:
                return status, ip, ip_ttl
        return False, None, None

    def _get_history_to_print(self, history, status, ip):
        message = 'Successfuly found the ip ' + ip if status else 'The ip was not found =('
        history = [{'Status' : f'{message} ; below you will see an exploration trace'}] + \
                  self._history + [{'Status' : message}]
        return history

    def clear(self):
        self._history = []
        self._no_ips_yet = []
        self._discovered_ips = set()

    def _try_resolve_with_unresolverd_ips(self, website_name, use_cache):
        for (unresolved_host, prev_name) in self._no_ips_yet:
            algo = Crawler() # self._discovered_ips
            _, new_ip = algo.find_ip(unresolved_host, use_cache=use_cache)
            if new_ip is None:
                continue
            self._history.append({'Found ip for unresolved host' : (unresolved_host, new_ip)})
            status, ip, _ = self._crawl_recursively(website_name, [(new_ip, prev_name)], use_cache)
            if status:
                return status, ip
        return False, None

    def find_ip(self, website_name, *, use_cache):
        global storage
        website_name = '.'.join([n for n in website_name.split('.') if n]).lower() # delete trailing dots

        if use_cache and storage.get(website_name)[0] is not None:
            ip = storage.get(website_name)[0]['ip']
            status = not (ip is None)
            is_found_str = 'not ' if ip is None else ''
            self._history.append({'Action' : f'Ip of a {website_name} extracted from cache (ip was {is_found_str}found)'})
        else:
            ips_to_explore = [(ip, '') for ip in _ROOT_SERVERS_IPS]
            status, ip, _ = self._crawl_recursively(website_name, ips_to_explore, use_cache)
            if not status and self._no_ips_yet:
                status, ip = self._try_resolve_with_unresolverd_ips(website_name, use_cache)

            if not status:
                storage.add(website_name, {'ip' : None, 'ip_ttl' : 60}, 'A')

        history_to_print = self._get_history_to_print(self._history, status, ip)
        self.clear()
        return history_to_print, ip
