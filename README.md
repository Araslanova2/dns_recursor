# DNS Recursor

## Examples

* Run `python3 api.py`
* Run one of the following commands in another tab
1) Process a DNS request

    `curl http://127.0.0.1:5000/dns/vk.ru?use_cache=true\&return_history=true`

Comments:

Write your own domain name instead of the 'vk.ru' above

Use argument `use_cache` to turn a cache on/off. A default value is `true`

Use argument `return_history` to return a search trace. A default value is `true`

2) Change cache size

    `curl -H "Content-Type: application/json" -X POST -d '{"new size":20}' http://127.0.0.1:5000/cache_size`

Wrire an integer >= 0 instead of the '20' above. A default cache size is 100

* As far as I understand, `trace=true` argument from the task is the following command:

`curl http://127.0.0.1:5000/dns/vk.ru?use_cache=false\&return_history=true`

## Project structure

| file name | description |
| ------ | ------ |
| create_request.py | creates a request for a given domain name |
| parse_response.py | parse a DNS-response into a dictionary |
| utils.py | helpers for the create_request.py and parse_response.py |
| crawler.py | the core of the dns-server, recursively search for an ip |
| cache.py | a cache to store crawler's results |
| api.py | a Flask-based user interface |
| ./examples/ | simple tests for public functions from create_request.py, parse_response.py, cache.py and crawler.py |

## Requirements

Python 3.8.5

flask 1.1.2

