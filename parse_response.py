from utils import hex_to_bits, convert_record_type, convert_record_class, get_octat_to_name, extract_name, read_ip

# According to the https://tools.ietf.org/html/rfc1035
# Pages 24-30

def _parse_flags(response):
    transaction_id = response[:2]
    flags = response[2 : 4]
    is_error = int(hex_to_bits(flags)[-3:], base=2)

    QDCOUNT = response[4 : 6]
    ANCOUNT_int = int(response[6 : 8].hex(), base=16)
    NSCOUNT_int = int(response[9 : 10].hex(), base=16)
    ARCOUNT_int = int(response[11 : 12].hex(), base=16)

    answer_body = response[12:]
    shift = 12
    return transaction_id, is_error, ANCOUNT_int, NSCOUNT_int, ARCOUNT_int, answer_body, shift

def _extract_query_info(answer_body, shift):
    name, answer_body, octat_to_name, cnt_in_name = extract_name(answer_body, shift, {})
    query_type = convert_record_type(answer_body[:2])
    query_class = convert_record_class(answer_body[2 : 4])
    query_info = {'name': name, 'type': query_type, 'class': query_class}

    answer_body = answer_body[4:]
    cnt_in_name += 4
    return (query_info, answer_body, octat_to_name, shift + cnt_in_name)

def _parse_records(answer_body, shift, octat_to_name, NSCOUNT_int):
    records = {}

    for response_id in range(NSCOUNT_int):
        name, answer_body, octat_to_name_request, cnt_in_name = extract_name(answer_body, shift, octat_to_name)
        octat_to_name.update(octat_to_name_request)

        if answer_body[0] == 0: # ducttape
            cnt_in_name += 1
            answer_body = answer_body[1:]

        query_type = convert_record_type(answer_body[:1])
        query_class = convert_record_class(answer_body[1 : 3])
        query_ttl = int(answer_body[3 : 7].hex(), base=16) # in seconds
        RDLENGTH = int(answer_body[7 : 9].hex(), base=16)
        name_server, _, octat_to_name_request, _ = extract_name(
                                                        answer_body[9 : 9 + RDLENGTH],
                                                        shift + cnt_in_name + 9,
                                                        octat_to_name)
        octat_to_name.update(octat_to_name_request)
        answer_body = answer_body[9 + RDLENGTH:]
        shift += cnt_in_name + 9 + RDLENGTH

        records[name_server.lower().strip('.')] = {
                                'name' : name.lower().strip('.'),
                                'type' : query_type, 'class' : query_class,
                                'ttl' : query_ttl, 'name_server' : name_server.lower().strip('.')}
    return (records, answer_body, shift, octat_to_name)

def _parse_ip_record(answer_body, shift, octat_to_name):
    name_server, answer_body, octat_to_name_request, cnt_in_name = extract_name(answer_body, shift, octat_to_name)
    octat_to_name.update(octat_to_name_request)

    if answer_body[0] == 0: # ducttape [for consistency with _parse_records]
        cnt_in_name += 1
        answer_body = answer_body[1:]

    query_type = convert_record_type(answer_body[:1])
    query_class = convert_record_class(answer_body[1 : 3])
    query_ttl = int(answer_body[3 : 7].hex(), base=16) # in seconds
    ip_length = int(answer_body[7 : 9].hex(), base=16)
    ip = read_ip(answer_body[9 : 9 + ip_length], ip_length)
    answer_body = answer_body[9 + ip_length:]
    shift += cnt_in_name + 9 + ip_length
    add_record_info = {'name_server' : name_server.lower().strip('.'),
                       'ip_type' : query_type, 'ip_class' : query_class,
                       'ip_ttl' : query_ttl, 'ip' : ip}
    return (add_record_info, answer_body, shift, octat_to_name)

def _parse_additional_records(answer_body, shift, octat_to_name, ARCOUNT_int):
    additional_records = []
    for additional_response_id in range(ARCOUNT_int):
        add_record_info, answer_body, shift, octat_to_name = _parse_ip_record(answer_body, shift, octat_to_name)
        additional_records.append(add_record_info)
    return (additional_records, answer_body, shift, octat_to_name)

# append additional_records into records
# return list of records
def _merge_records_and_additional_records(records, additional_records):
    for info in additional_records:
        name_server = info['name_server']
        if 'ip' not in records[name_server]:
            records[name_server]['ip'] = []
        records[name_server]['ip'].append(info)
    return list(records.values())

def parse_response(response):
    # `shift` is currect byte (octate)
    # `answer_body` is the unparsed bytes
    transaction_id, is_error, ANCOUNT_int, NSCOUNT_int, ARCOUNT_int, answer_body, shift = _parse_flags(response)

    if is_error != 0:
        return transaction_id, None, None

    query_info, answer_body, octat_to_name, shift = _extract_query_info(answer_body, shift)
    if ANCOUNT_int > 0:
        ip_info, answer_body, shift, octat_to_name = _parse_ip_record(answer_body, shift, octat_to_name)
        return transaction_id, query_info, ip_info

    records, answer_body, shift, octat_to_name = _parse_records(answer_body, shift, octat_to_name, NSCOUNT_int)
    additional_records, answer_body, shift, _octat_to_name = _parse_additional_records(answer_body, shift, octat_to_name, ARCOUNT_int)
    merged_records = _merge_records_and_additional_records(records, additional_records)

    return transaction_id, query_info, merged_records
