from random import randint

from utils import bits_to_hex, hex_to_bits, encode_name_to_hex_byte_str

# According to the https://tools.ietf.org/html/rfc1035
# Pages 24-30
'''
                                    1  1  1  1  1  1
      0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                      ID                       |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |QR|   Opcode  |AA|TC|RD|RA|   Z    |   RCODE   |
'''
def _get_request_header():
    # transaction_id is a random 16-bits number (2^16 = 65536 possible values)
    transaction_id = hex_to_bits(randint(0, pow(2, 8) - 1)) + hex_to_bits(randint(0, pow(2, 8) - 1))
    flags = '0' * 7 + '1' + '0' * (16 - 7 - 1) # recursion desired
    question_cnt =  '0' * 15 + '1' + '0' * (16 * 4 - 15 - 1) # one question
    return bits_to_hex(transaction_id + flags + question_cnt), transaction_id

# example create_request('academy.yandex.ru', 'NS')
def create_request(website_name, request_type):
    message, transaction_id = _get_request_header()
    message += encode_name_to_hex_byte_str(website_name)

    if request_type == 'A':
        message += bits_to_hex('0' * 15 + '1') # A, type 1
    elif request_type == 'NS':
        message += bits_to_hex('0' * 14 + '10') # NS, type 2
    else:
        print("Unknown request type in `create_request`")
    message += bits_to_hex('0' * 15 + '1') # class internet
    return transaction_id, message
