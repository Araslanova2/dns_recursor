import time

from random import randint

class A_Record:
    def __init__(self, data):
        self.record_time = time.time()
        self.data = data
        self.type = 'A'

    def get(self):
        ttl_diff = self.data['ip_ttl'] - (time.time() - self.record_time)
        if ttl_diff <= 0:
            return {}
        self.data['ip_ttl'] = ttl_diff
        return self.data

class NS_Record:
    def __init__(self, data):
        self.record_time = time.time()
        self.data = data
        self.part_expired = False
        self.type = 'NS'

    # return non-expired data
    def _clear_and_get(self):
        ids_to_delete = []
        for idx, record in enumerate(self.data):
            ttl_diff = record['ttl'] - (time.time() - self.record_time)
            if ttl_diff <= 0:
                ids_to_delete.append(idx)
            else:
                self.data[idx]['ttl'] = ttl_diff
        for idx in ids_to_delete[::-1]:
            self.data.pop(idx)
            self.part_expired = True
        return self.data

    # return non-expired data, only records with the given record['name'] in `tail_to_check`
    def get(self, tail_to_check = None):
        if tail_to_check is None:
            return self._clear_and_get()
        records = []
        for record in self.data:
            if tail_to_check.endswith(record['name']):
                records.append(record)
        return records

class Cache:
    def __init__(self, cache_size):
        self.cache_size = max(cache_size, 0) # max number of data records
        self.cnt_records_in_cache = 0
        self.name_to_record = {}

    def _try_delete_expired_record(self):
        for (name, data) in self.name_to_record.items():
            if not data.get():
                del self.name_to_record[name]
                return True
        return False

    def _delete_random_record(self):
        keys = list(self.name_to_record.keys())
        key_id = randint(0, len(keys) - 1)
        del self.name_to_record[keys[key_id]]
        self.cnt_records_in_cache -= 1

    def _decrease_size_by_one(self):
        if not self._try_delete_expired_record():
            self._delete_random_record()

    def set_max_size(self, new_cache_size):
        if new_cache_size < 0:
            return

        self.cache_size = new_cache_size
        while new_cache_size < self.cnt_records_in_cache:
            self._decrease_size_by_one()


    # request_type ('A' ot 'NS') needs to process get correctly: return an ip or a set of records
    # `name` for 'A' : website
    # `name` for 'NS' : ip
    def add(self, name, data, request_type):
        if self.cache_size == 0:
            return

        if self.cnt_records_in_cache == self.cache_size:
            self._decrease_size_by_one()

        if name not in self.name_to_record:
            self.name_to_record[name] = []

        record = A_Record(data) if request_type == 'A' else NS_Record(data)
        self.name_to_record[name] = record
        self.cnt_records_in_cache += 1

    def get(self, name, tail_to_check=None):
        if name not in self.name_to_record:
            return None, None
        if self.name_to_record[name].get():
            if self.name_to_record[name].type == 'NS':
                if self.name_to_record[name].get(tail_to_check):
                    return self.name_to_record[name].get(tail_to_check), self.name_to_record[name].part_expired
                else:
                    return None, None # no ns-record for the given website
            elif self.name_to_record[name].type == 'A':
                return self.name_to_record[name].get(), None

        del self.name_to_record[name]
        return None, None
