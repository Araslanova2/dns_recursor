from flask import Flask, jsonify, request

from crawler import Crawler, storage

app = Flask(__name__)

cache_size = 10

# curl -H "Content-Type: application/json" -X POST -d '{"new size":20}' http://127.0.0.1:5000/cache_size

@app.route("/cache_size", methods=['POST'])
def update_cache_size():
    global storage
    new_size = request.json["new size"]
    if type(new_size) == int and new_size >= 0:
        storage.set_max_size(new_size)
        return jsonify({"Result" : f"Cache size was changed to {new_size}"}), 200
    return jsonify({"Result" : "Cache size should an int be >= 0"}), 422

# curl http://127.0.0.1:5000/dns/vk.ru?use_cache=true\&return_history=true

@app.route("/dns/<string:website>", methods=['GET'])
def get_ip(website):
    use_cache = request.args.get('use_cache', True)
    use_cache = False if use_cache in ['false', 'False'] else True

    return_history = request.args.get('return_history', True)
    return_history = False if return_history in ['false', 'False'] else True

    history_to_print, ip = Crawler().find_ip(website, use_cache = use_cache)

    return_status = 200 if ip is not None else 404
    ip = ip if ip is not None else 'ip was not found'

    if return_history:
        return jsonify({"ip" : ip, "history" : history_to_print}), return_status
    return jsonify({"ip" : ip}), return_status

if __name__ == '__main__':
    app.run(debug=True)
